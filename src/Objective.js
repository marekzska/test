import React, { useState, useRef } from "react";
import "./objective.css";

export default function Objective(props) {
  const [showDescription, setShowDescription] = useState(false);
  const answerVal = useRef("");
  const mountedStyle = {
    animation: "inAnimation 300ms ease-in",
  };

  function handleObjectiveClick(e) {
    e.preventDefault();
    setShowDescription((prevVal) => !prevVal);
  }

  function handleEnter(e) {
    if (e.key.toLowerCase() === "enter") {
      e.preventDefault();
      handleSubmitClick(e);
    }
  }

  function handleSubmitClick(e) {
    e.preventDefault();
    if (
      answerVal.current.value.toLowerCase() === props[1].answer.toLowerCase()
    ) {
      if (props.index === 0) {
        alert(
          "Well done! Now you get the point. Find information about my coworkers and find the mole."
        );
      }
      fetch("https://marekzska.itervitae.eu/PHP/update.php", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: "value=" + props.index,
      })
        .then((response) => response.json())
        .then((data) => console.log(data))
        .catch((err) => console.log(err));
      if (props.index === 5) {
        alert(
          "Well done! Find the contraband in the electric box opposite the shoe cabinet."
        );
      } else {
        props.fetchObjectivesAgain();
      }
    } else {
      answerVal.current.value = "";
    }
    setShowDescription((prevVal) => !prevVal);
  }

  return (
    <li className='objectiveLine'>
      <div
        className={`titleWrap ${showDescription && "activeObjective"} ${
          props[1].solved === "1" && "solved"
        } ${props[1].show !== "1" ? "unshow" : ""}`}
        onClick={handleObjectiveClick}
      >
        <h1 className='objectiveTitle'>{props[1].title}</h1>
        <i className='fa fa-angle-down'></i>
      </div>
      {showDescription && (
        <div
          className='descriptionWrap'
          style={showDescription ? mountedStyle : ""}
        >
          <div className='description'>{props[1].description}</div>
          {props[1].solved === "0" && (
            <div className='answerForm'>
              <input
                className='answerBox'
                type='text'
                name='answer'
                onKeyDown={handleEnter}
                ref={answerVal}
                placeholder='answer'
              />
              <input
                className='answerSubmit'
                type='submit'
                value='Submit'
                onClick={handleSubmitClick}
              />
            </div>
          )}
        </div>
      )}
    </li>
  );
}
