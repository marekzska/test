import React, { useEffect, useState, useLayoutEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./dashboard.css";
// import Objective from "./Objective.js";
import Credentials from "./Credentials.js";
import ProfilePic from "./ProfilePic.js";
import ObjectiveList from "./ObjectiveList.js";

export default function Dashboard() {
  const [objectives, setObjectives] = useState({});
  const [loggedIn, setLoggedIn] = useState(() =>
    localStorage.getItem("isLoggedIn")
  );
  const navigate = useNavigate();
  const[caseFiles, setCaseFiles] = useState(0);

  useLayoutEffect(() => {
    fetch("https://marekzska.itervitae.eu/PHP/return.php")
      .then((response) => response.json())
      .then((data) => {
        setObjectives(data);
      })
      .catch((error) => console.log(error));

    //uncommentbeforedeployment

    // const logOut = setTimeout(() => {
    //   setLoggedIn("false");
    //   navigate(-1);
    // }, 600000);

    // return () => {
    //   clearTimeout(logOut);
    // };
  }, []);

  useEffect(() => {
    localStorage.setItem("isLoggedIn", loggedIn);
  }, [loggedIn]);

  function fetchObjectivesAgain() {
    fetch("https://marekzska.itervitae.eu/PHP/return.php")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setObjectives(data);
      })
      .catch((error) => console.log(error));
  }

  function handleButtonClick() {
    setLoggedIn("false");
    navigate(-1);
  }

  if (loggedIn === "true") {
    return (
      <>
        <div id='centerClass'>
          <div id='agentInfo'>
            <ProfilePic />
            <Credentials />
          </div>

          <div id='objectiveList'>
            {Object.keys(objectives).length > 0 && (
              <ObjectiveList objectives={objectives} fetchObjectivesAgain={fetchObjectivesAgain}  />
            )}
          </div>
        </div>

        <button
          title='Log Out'
          onClick={handleButtonClick}
          value='ano'
          id='logout'
          name='ano'
        >
          <img
            id='logOutImg'
            src='https://cdn-icons-png.flaticon.com/512/66/66882.png?w=360'
            alt='log out'
          />
        </button>
      </>
    );
  }
}
