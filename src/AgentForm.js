import React, { useState, useRef } from "react";
import "./agentform.css";
import { useNavigate } from "react-router-dom";

export default function AgentForm() {
  const [pin, setPin] = useState("");
  const [password, setPassword] = useState("");
  const inputReference = useRef();
  const navigate = useNavigate();

  function handlePinChange(e) {
    setPin(e.target.value);
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleEnter(e) {
    if (e.key.toLowerCase() === "enter") {
      e.preventDefault();
      const form = e.target.form;
      const index = [...form].indexOf(e.target);
      form.elements[index + 1].focus();
    }
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    if (pin === "1512" && password.toUpperCase() === "NIGHCMAOUJVCMBW") {
      // alert("ANOOO");
      window.localStorage.setItem("isLoggedIn", "true");
      navigate("/dashboard");
    } else if (pin === "1066" && password.toLowerCase() === "greenwich") {
      window.localStorage.setItem("isLoggedIn", "true");
      navigate("/janice");
    }
    setPassword("");
    setPin("");
    inputReference.current.focus();
  }

  return (
    <>
      <form id='loginForm' action='#' onSubmit={handleFormSubmit}>
        <fieldset>
          <input
            type='password'
            name='pin'
            className='loginInput'
            pattern='[0-9]{4}'
            placeholder='PIN'
            maxLength='4'
            value={pin}
            autoFocus
            onChange={handlePinChange}
            onKeyDown={handleEnter}
            ref={inputReference}
          />
          <input
            name='password'
            className='loginInput'
            type='password'
            placeholder='PASSWORD'
            value={password}
            onChange={handlePasswordChange}
          />

          <input type='submit' id='submit' />
        </fieldset>
      </form>
    </>
  );
}
