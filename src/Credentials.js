import React from 'react';
import "./credentials.css";

export default function Credentials(){

    return (
      <ul id='credentials'>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Legal Name:</h3>
          <h3 className='credentialItem'>G.D.</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Alias:</h3>
          <h3 className='credentialItem'>Kingfisher</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Age:</h3>
          <h3 className='credentialItem'>29</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Clearance:</h3>
          <h3 className='credentialItem'>AA - Secret</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Classification:</h3>
          <h3 className='credentialItem'>Counter-smuggling unit</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Deployment:</h3>
          <h3 className='credentialItem'>Paris, France</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Status:</h3>
          <h3 className='credentialItem'>Active</h3>
        </li>
      </ul>
    );
}